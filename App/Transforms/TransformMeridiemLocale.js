import moment from 'moment'
//Mise à jour du AM en Matin et PM en Après-midi
moment.updateLocale('fr', {
  meridiem : function (hour, minute, isLowercase) {
    let term = hour <= 12 ? "Matin" : "Après-midi";
    return isLowercase ? term.toLowerCase() : term;
  }
});

//Mise à jour du AM <= 12h et PM Après
moment.updateLocale('en', {
  meridiem: function (hour, minute, isLowercase) {
    let term = hour <= 12 ? "AM" : "PM";
    return isLowercase ? term.toLowerCase() : term;
  }
});