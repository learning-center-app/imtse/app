export default SortSectionnedListByUtilisateurCampus = (array, campus_id) => {
  return array.reduce((acc, campus) => {
    //Placement du campus concernant l'utilsateur connecté en premier
    //https://stackoverflow.com/a/37551373/1437016
    if (campus_id && campus.campus_id == campus_id)
      return [campus, ...acc];
    return [...acc, campus];
  }, [])
}
