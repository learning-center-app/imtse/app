import Salle from '../Classes/Salle'

export default UpdateReservationsInSalles = (reservations_sorted_by_salles, salles) => {
  //Anti crash si pas de salles synchros
  if (!salles || salles.length == 0)
    return [];
    
  //On va muter le tableau salles pour y inclure les résas
  // deep : true pour que les objets dans le tableau soient aussi mutables
  //asMutable est une fonction de la librairie Immutable
  var _salles = salles.map((salle) => {
    //Inclusion des résas dans les salles
    let reservations = reservations_sorted_by_salles[salle.id];
    //S'il n'y a pas de résa, on y met un tableau vide
    reservations = typeof reservations != "undefined" ? reservations.data : [];
    return new Salle(salle, reservations);
  })

  //Renvoie les salles en format brut ET en sorted, via un autre transform
  return _salles
}
