export default SortReservationsBySalles = (reservations) => {
  var data = [];

  let obj = reservations.reduce((result, item) => {
    if(typeof result[item.room_id] == "undefined")
      result[item.room_id] = {id_salle:item.room_id, name_salle: item.salle, name_campus:item.campus, data: []}
    result[item.room_id].data.push(item);
    return result;
  }, {})

  return obj;
}
