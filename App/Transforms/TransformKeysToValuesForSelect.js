export default TransformKeysToValuesForSelect = (object) => {
  var data = [];

  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      data.push({
        label : object[key],
        value : key
      })
    }
  }
  // __DEV__ && console.tron.log(data);
  return data;
}
