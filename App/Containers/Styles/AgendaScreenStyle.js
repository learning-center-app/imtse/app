import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    marginBottom: 20
  },
  monthContainer: {
    backgroundColor: 'white',
    padding: 12,
    paddingLeft: 30,
    marginTop: 0,
    marginBottom: 15,
  },
  container_timeline: {
    marginTop: 20,
  },
  time_timeline: {
    minWidth: 50,
    marginTop: -5,
    marginRight: -8,
  },
  time_text_timeline: {
    fontSize: 18,
    color: 'white',
    textAlign: 'right',
  },
  title_timeline: {
    fontSize: 18,
    fontWeight: 'normal',
    color: 'white',
    padding: 0,
    margin: 0,
    marginBottom: 5,
  },

  description_timeline: {
    fontSize: 18,
  },

  details_timeline: {
    flex: 1,
    backgroundColor: Colors.transparent,
    margin: 0,
    marginLeft: -8,
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginBottom: 30,


  },
  details_row_timeline: {
    flex: 1,
    backgroundColor: 'red',

  },

  detailContainer:{
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
    marginRight: 20
  },
  detailTitle:{
    fontSize:16,
    fontWeight: 'bold'
  },
  detailDescriptionContainer:{
    flexDirection: 'row',
    paddingRight: 50,
  },
  detailImage:{
    width: 50,
    height: 50,
    borderRadius: 25
  },
  detailTextDescription: {
    marginLeft: 10,
    color: 'gray'
  },
  modalTitle: {
    fontSize: Fonts.size.h3,
    padding:20
  },
  modalDate: {
    fontSize: Fonts.size.h6,
    color: Colors.white,
    //padding:20
  },
  modalDesc: {
    fontSize: Fonts.size.h6,
    color: Colors.white,
    //padding:20
  },
  scrollView : {
    // flex:1,
    maxHeight: 200,
    // paddingVertical: 20
  }
})
