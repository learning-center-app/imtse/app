import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Images } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  main_container: {
    flex: 1,
    backgroundColor: Colors.transparent
  },
  pagecontrol_container: {
    position:'absolute',
    zIndex: 20,
    bottom:20,
    left:20,
    right:20,
  },
  viewpager_container: {
    flex: 1
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'cover'
  },
  centered: {
    alignItems: 'center'
  },
  didactImg: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover'
  }
})
