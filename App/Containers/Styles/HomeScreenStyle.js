import { Platform, StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    // alignItems:'center',
  },
  row: {
    marginVertical: Metrics.smallMargin,
    justifyContent: 'center',
    alignItems: 'center',
    height:210,
    width:210,
    marginBottom:-100,
    borderRadius: 200,
  },
  button_block: {
    justifyContent: 'center',
    alignItems: 'center',
    height:150,
    width:150,
  },
  rotate : {
    transform: [
      {rotate: (Platform.OS === 'ios') ? '45deg' : (3.14159/4)+'rad'}
    ]
  },
  inverse_rotate : {
    transform: [
      {rotate: (Platform.OS === 'ios') ? '-45deg' : (-3.14159/4)+'rad'}
    ]
  },
  button : {
    width:'100%',
    height:'100%',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:'red',
  },
  item_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  item_title : {
    alignSelf: 'center',
    textAlign: 'center',
    maxWidth:'70%',
    paddingTop:2,
    paddingBottom: 16,

    fontFamily: Fonts.type.base,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
    fontSize: 16,
    color: Colors.text,
  },
  header : {
    marginBottom:120
  },
  odd: {
    marginLeft:'0%',
    marginRight:120,
  },
  even: {
    marginLeft:120,
  },
  icon: {
    // position: 'absolute',
    // top:10,
    // left:10,
    // bottom:10,
    // right:10,
    resizeMode: 'contain',
    width:80,
    height:80,
  },
  boldLabel: {
    fontWeight: 'bold',
    alignSelf: 'center',
    color: Colors.snow,
    textAlign: 'center',
    marginBottom: Metrics.smallMargin
  },
  label: {
    textAlign: 'center',
    color: Colors.snow
  },
  listContent: {
    paddingBottom: 120,
    alignItems:'center',
  },
  menu_icon : {
    marginLeft:10,
    resizeMode: 'contain',
    width:24,
    height:24,
    paddingHorizontal:Metrics.doubleBaseMargin,
    paddingVertical:Metrics.baseMargin,
  }
})
