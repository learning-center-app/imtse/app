//@flow
import { StyleSheet } from 'react-native'
import { Metrics, Colors, ApplicationStyles } from '../../Themes/'
// import Colors from '../../Themes/Colors'
export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.components,
  ...ApplicationStyles.forms,
  centerText: {
    textAlign:'center'
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  textInput: {
    marginBottom:30,
  },
  fullWidth:{
    width:'100%',
    padding:10,
    paddingLeft:30,
    fontSize:20
  },
  whiteText:{
    color:Colors.white,
  },
  align_center: {
    alignSelf: 'center',
  },
  activity_indicator : {
    zIndex:10,
    position:'absolute',
    top:0,
    bottom:0,
    left:0,
    right:0,
  },
  menu_icon : {
    marginRight:10,
    resizeMode: 'contain',
    width:24,
    height:24,
    paddingHorizontal:Metrics.baseMargin,
  },
})
