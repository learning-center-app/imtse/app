import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.forms,
  ...ApplicationStyles.components,

  webview: {
    flex:1,
    backgroundColor:Colors.transparent,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: Metrics.doubleBaseMargin * 2,
  },
  padded : {
    paddingVertical: Metrics.doubleBaseMargin * 2,
    paddingHorizontal: Metrics.doubleBaseMargin,
  },
  margedVertical : {
    marginTop: Metrics.doubleBaseMargin * 2,
  }
})
