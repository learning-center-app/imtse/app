import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.forms,
  ...ApplicationStyles.components,
  fullWidth:{
    width:'100%',
    padding:10,
    paddingLeft:20
  },
  whiteBg:{
    backgroundColor:Colors.white,
  },
  spacer:{
    marginTop:4
  },
  multilineText:{
     backgroundColor: Colors.rdv_dark,
     color:'#ffffff',
     paddingLeft:20,
     minHeight:120
  },
  messageHeader:{
    paddingLeft:20,
    marginTop:10,
    paddingBottom:5
  },
  textInput:{
    backgroundColor:Colors.rdv_dark,
    minHeight:30,
    paddingLeft:20,
    color:Colors.white
  },
})
