import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts} from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.components,
  ...ApplicationStyles.forms,
  fullWidth:{
    width:"100%",
    paddingLeft:20,
    paddingTop:8,
    paddingBottom:8
  },

  button_block: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
  },
  icon: {
    // position: 'absolute',
    // top:10,
    // left:10,
    // bottom:10,
    // right:10,
    resizeMode: 'contain',
    width: 70,
    height: 70,
  },
  boldLabel: {
    ...Fonts.style.h4,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: Colors.snow,
    textAlign: 'center',
    marginTop: Metrics.smallMargin
  },
  multilineText:{
     backgroundColor: '#000000',
     padding:20,
     minHeight:120
  },
  marginTop: {
    marginTop:10,
  },
  marginTop3: {
    marginTop: 30,
  },

  container:{
    flex:1
  }
})
