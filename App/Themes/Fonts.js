import Colors from './Colors'
const type = {
  base: 'Klima-Light',
  light: 'Klima-Light',
  bold: 'Klima-Bold',
  emphasis: 'Klima-RegularItalic'
}

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 8.5
}

const style = {
  h1: {
    color: Colors.text,
    fontFamily: type.light,
    fontSize: size.h1,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h2: {
    color: Colors.text,
    fontFamily: type.light,
    fontSize: size.h2,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h3: {
    color: Colors.text,
    fontFamily: type.light,
    fontSize: size.h3,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h4: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.h4,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h5: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.h5,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h6: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.h6,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  normal: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.regular,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  small: {
    color: Colors.slightlyTransparentText,
    fontFamily: type.base,
    fontSize: size.small,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  description: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.medium,
    fontWeight:'300',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  }
}

export default {
  type,
  size,
  style
}
