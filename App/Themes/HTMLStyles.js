import {StyleSheet} from 'react-native';
import Fonts from './Fonts'
import Metrics from './Metrics'
import Colors from './Colors'

export default StyleSheet.create({
  a: {
    ...Fonts.style.description,
    color: Colors.info,
  },
  p: {
    ...Fonts.style.description,
    marginVertical: Metrics.baseMargin,
  },
  div: {
    ...Fonts.style.description,
  },
  span: {
    ...Fonts.style.description,
  },
  ul: {
    ...Fonts.style.description,
  },
  ol: {
    ...Fonts.style.description,
  },
  li: {
    ...Fonts.style.description,
  }
});
