import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { TouchableOpacity, Text } from 'react-native'
import styles from './Styles/SemanticButtonStyle'
import { Colors } from '../Themes'

export default class SemanticButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bgColor : this.getColorFromType(props.type)
    }
  }
  getColorFromType = (type) => {
    let color;
    switch (type) {
      case 'info':
        color = Colors.info;
        break;
      case 'warning':
        color = Colors.warning;
        break;
      case 'success':
        color = Colors.success;
        break;
      case 'error':
        color = Colors.error;
        break;
      default:
        color = 'info';
        break;
    }
    return { backgroundColor: color };
  }
  // bgColor = {}

  static propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    onPress: PropTypes.func,
    styles: PropTypes.object,
    size: PropTypes.string,
  }

  render () {
    return (
      <TouchableOpacity disabled={this.props.disabled || false} style={[styles.button, this.props.styles, this.state.bgColor, (this.props.size == 'small' && styles.button_small), this.props.disabled && {"opacity" : 0.3}]}  onPress={this.props.onPress}>
        <Text style={[styles.buttonText,  (this.props.size == 'small' && styles.button_text_small)]}>{this.props.text.toUpperCase()}</Text>
      </TouchableOpacity>
    )
  }
}
