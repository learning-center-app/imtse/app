//@flow
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Animated } from 'react-native'
import styles from './Styles/CollapsableItemStyle'

const MIN_HEIGHT = 100;

export default class CollapsableItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      animation : new Animated.Value(MIN_HEIGHT),
      minHeight:MIN_HEIGHT || props.minHeight
    };
  }

  // // Prop type warnings
  static propTypes = {
    backgroundColor: PropTypes.string,
    minHeight: PropTypes.number,
  }

  _setMaxHeight(event){
    this.setState({
        maxHeight   : event.nativeEvent.layout.height
    });
  }

  _setMinHeight(event){
    this.setState({
        minHeight   : event.nativeEvent.layout.height
    });
  }
  toggle(){
    //Step 1
    let initialValue    = this.state.expanded? this.state.maxHeight : this.state.minHeight,
        finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight;

    this.setState({
        expanded : !this.state.expanded  //Step 2
    });

    this.state.animation.setValue(initialValue);  //Step 3
    Animated.spring(     //Step 4
        this.state.animation,
        {
            toValue: finalValue
        }
    ).start();  //Step 5
  }

  render () {
    return (
      <Animated.View style={[styles.container, {backgroundColor:this.props.backgroundColor}, {height: this.state.animation}]} >
        <TouchableOpacity onPress={() => this.toggle()} onLayout={this._setMaxHeight.bind(this)}>
          {this.props.children}
        </TouchableOpacity>
      </Animated.View>

    )
  }
}
