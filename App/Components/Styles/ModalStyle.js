import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  modal_container: {
    // flex: 1,
    backgroundColor: Colors.transparent,
    padding:20
  },
  modal_title: {
    ...Fonts.style.h4
  },
  modal_view: {
    paddingHorizontal:10,
    paddingVertical:20,
  },
  modal_actions: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center'
  },

})
