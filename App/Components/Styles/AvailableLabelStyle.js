import { StyleSheet } from 'react-native'
import { Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  base : {
    ...Fonts.style.normal,
    textAlign:'center',
  },
  white_block : {
    paddingVertical: 2,
    paddingHorizontal: 12,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 10,
    overflow:'hidden',
  },
  inline:{
    flexDirection:'row',
  }
})
