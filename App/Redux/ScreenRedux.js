import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  updateScreenDescription: ['list', 'description']
})

export const ScreenTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  list: [
      {
        icon: 'dispo_salle',
        color: 'rgb(0, 169, 224)', //https://www.pantone.com/color-finder/2995-c
        title : "Réservation salle 1 heure",
        description : "Description",
        needs_login: false,
        screen : "DispoSalleScreen"
      },
      {
        icon: 'reservation_salle',
        color: 'rgb(0, 79, 113)', //https://www.pantone.com/color-finder/3025-c
        title : "Réservation de salle",
        description : "Description",
        needs_login: true,
        screen : "MesReservationsSalleScreen"
      },
      {
        icon: 'reservation_materiel',
        color: 'rgb(95, 36, 159)', //https://www.pantone.com/color-finder/267-c
        title : "Réservation de matériel",
        description : "Description",
        needs_login: true,
        screen : "MesReservationsMaterielScreen"
      },
      {
        icon: 'emprunt_livre',
        color: 'rgb(234, 118, 0)', //https://www.pantone.com/color-finder/716-c
        title : "Prêt de livre",
        description : "Description",
        needs_login: true,
        screen : "EmpruntLivreScreen"
      },
      {
        icon: 'demande_reference',
        color: 'rgb(219, 123, 81)', //https://www.pantone.com/color-finder/2433-C
        // color: 'rgb(255, 209, 0)', //https://www.pantone.com/color-finder/109-c
        title: "Demande de réference",
        description: "Description",
        needs_login: true,
        screen: "DemandeReferenceScreen"
      },
      {
        icon: 'suggestion',
        color: 'rgb(147, 40, 44)', //https://www.pantone.com/color-finder/7622-c
        title : "Suggestion",
        description : "Description",
        needs_login: true,
        screen : "SuggestionScreen"
      },
      {
        icon: 'rendez_vous',
        color: 'rgb(98, 52, 18)', //https://www.pantone.com/color-finder/732-C
        // color: 'rgb(224, 98, 135)', //https://www.pantone.com/color-finder/7423-c
        title : "Rendez-vous",
        description : "Description",
        needs_login: true,
        screen : "RendezVousScreen"
      },
      {
        icon: 'agenda',
        color: 'rgb(74, 120, 60)', //https://www.pantone.com/color-finder/7742-c
        // color: 'rgb(151, 215, 0)', //https://www.pantone.com/color-finder/375-c
        title : "Agenda",
        description : "Description",
        needs_login: false,
        screen : "AgendaScreen"
      },
      {
        icon: 'horaires',
        // color: 'rgb(74, 120, 60)', //https://www.pantone.com/color-finder/7742-c
        color: 'rgb(106, 144, 51)', //https://www.pantone.com/color-finder/4213-C
        title : "Horaires",
        description : "Description",
        needs_login: false,
        screen : "HorairesScreen"
      },
      {
        icon: 'actualites',
        color: 'rgb(155, 155, 155)', // GRIS 37% NOIR
        title : "Actualités",
        description : "Description",
        needs_login: false,
        screen : "ActualitesScreen"
      },
      {
        icon: 'description',
        color: 'rgb(24, 33, 36)',
        title: "Description",
        description: "Description",
        needs_login: false,
        screen: "DescriptionScreen"
      },
    ],
    menu : [
      {
        color: 'rgb(0,0,0)',
        title : "Tutoriel",
        description : "Conditions générales",
        needs_login: false,
        screen : "LaunchScreen"
      },
      {
        color: 'rgb(0,0,0)',
        title : "CGU",
        description : "Conditions générales d'utilisation",
        needs_login: false,
        screen : "CguScreen"
      },
      {
        color: 'rgb(0,0,0)',
        title : "Mentions légales",
        description : "Mentions légales",
        needs_login: false,
        screen : "MentionsLegalesScreen"
      },
      // {
      //   color: 'rgb(0,0,0)',
      //   title : "Signaler un bug",
      //   description : "Signaler un bug",
      //   needs_login: false
      // },
    ]
}

/* ------------- Selectors ------------- */

export const ScreenSelectors = {
  // selectAvatar: state => state.github.avatar
}

/* ------------- Reducers ------------- */

// successful avatar lookup
export const updateScreenDescription = (state, action) => {
  const { screen, description } = action;
  console.log(action);
  console.log(state);
  screen.description = description;
  debugger
  return state.merge({ list: [screen]})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_SCREEN_DESCRIPTION]: updateScreenDescription
})
