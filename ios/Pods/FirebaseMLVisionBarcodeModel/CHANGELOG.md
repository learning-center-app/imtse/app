# 2020-06-02 -- v0.20.0
- Bug fixes.

# 2019-10-08 -- v0.19.0
- An enhanced version of barcode scanner with better speed and accuracy.
